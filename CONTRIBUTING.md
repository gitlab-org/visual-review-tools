This project is developed in vanilla Javascript only. Because we are asking our users to inject this toolbar into their application for review,
it is important to keep things small and avoid dependencies on large scripts or other loads. As we continue to develop, we will revisit these
decisions, but for now, please use plain JS in your contributions.

## Automatic deploys

We use [`semantic-release`](https://gitlab.com/gitlab-org/gitlab-ui/wikis/Frequently-asked-questions#2-why-are-we-using-semantic-release) to automatically publish `gitlab-@gitlab/visual-review-tools`.
Please follow [this specification](https://www.conventionalcommits.org/en/v1.0.0-beta.2/#specification) to ensure your changes will be automatically deployed.

When opening an MR, make sure your changes are descibed by at least one commit message following conventional commit conventions,
and/or that the MR's title itself follows those conventions. Following these conventions will result in a properly versioned package and clear changelogs for every version.

Here are the commit types we recommend you use for your commit messages:

```
feat:     A new feature
fix:      A bug fix
docs:     Documentation only changes
style:    Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
refactor: A code change that neither fixes a bug nor adds a feature
perf:     A code change that improves performance
test:     Adding missing tests or correcting existing tests
build:    Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
ci:       Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
chore:    Other changes that don't modify src or test files
revert:   Reverts a previous commit
```

> Note that only `feat:` and `fix:` types trigger a new release

### Commitizen

https://commitizen.github.io/cz-cli/

Commitizen is a CLI tool that provides an interactive interface to help you write commit messages following conventional commits specifications.
