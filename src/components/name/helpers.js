import { clearNote, postError } from '../note/helpers';
import { ADD_NAME, ADD_EMAIL, COMMENT_BOX } from '../../shared';
import { addForm } from '../wrapper/helpers';
import { nextView } from '../../store/view';

export { saveName } from './storage';

export const isNameValid = ({ userName }) => {
  if (!userName) {
    postError('Your name appears to be empty.', ADD_NAME);
    return false;
  }
  clearNote();
  return true;
};

export const isEmailValid = ({ userEmail }) => {
  // Email is optional
  if (userEmail) {
    // Basic regex validity check
    const emailRegex = RegExp(/^\S+@\S+\.\S+$/);
    if (!emailRegex.test(userEmail)) {
      postError('Your email appears to be invalid.', ADD_EMAIL);
      return false;
    }
  }
  return true;
};

export const goToFeedbackForm = state => {
  addForm(nextView(state, COMMENT_BOX));
};
