import { EXPANSION_BUTTON } from '../../shared';
import { selectContainer, selectExpansionButtonContainer, selectForm } from '../utils';
import { expandRightIcon, collapseRightIcon } from './icons';

const addForm = nextForm => {
  const formWrapper = selectForm();
  formWrapper.innerHTML = nextForm;
};

const toggleExpansion = state => {
  const container = selectContainer();
  container.classList.toggle('gl-right-0');
  container.classList.toggle('gl-collapsed');
  state.isExpanded = !state.isExpanded;
  const expansionButtonContainer = selectExpansionButtonContainer();
  if (state.isExpanded) {
    expansionButtonContainer.innerHTML = collapseRightIcon(EXPANSION_BUTTON);
  } else {
    expansionButtonContainer.innerHTML = expandRightIcon(EXPANSION_BUTTON);
  }
};

export { addForm, toggleExpansion };
