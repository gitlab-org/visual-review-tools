// component selectors
const ADD_NAME = 'gitlab-add-name';
const ADD_NAME_BUTTON = 'gitlab-add-name-button';
const ADD_EMAIL = 'gitlab-add-email';
const ADD_NAME_CANCEL_BUTTON = 'gitlab-add-name-cancel-button';
const ADD_PAT = 'gitlab-add-pat';
const ADD_PAT_BUTTON = 'gitlab-add-pat-button';
const ADD_PAT_CANCEL_BUTTON = 'gitlab-add-pat-cancel-button';
const CHANGE_MR_ID_BUTTON = 'gitlab-change-mr';
const COMMENT_BOX = 'gitlab-comment';
const COMMENT_BUTTON = 'gitlab-comment-button';
const EXPANSION_BUTTON = 'gitlab-expansion-button';
const EXPANSION_BUTTON_CONTAINER = 'gitlab-expansion-button-container';
const EXPANSION_CONTAINER = 'gitlab-expansion-container';
const FORM = 'gitlab-form';
const FORM_CONTAINER = 'gitlab-form-wrapper';
const MR_ID = 'gitlab-submit-mr';
const MR_ID_BUTTON = 'gitlab-submit-mr-button';
const MR_ID_CANCEL_BUTTON = 'gitlab-cancel-mr-button';
const NAME_FIELDS_CONTAINER = 'gitlab-name-fields-wrapper';
const NOTE = 'gitlab-validation-note';
const NOTE_CONTAINER = 'gitlab-note-wrapper';
const PAT_FIELD_CONTAINER = 'gitlab-pat-field-wrapper';
const REMEMBER_ITEM = 'gitlab-remember-item';
const REVIEW_CONTAINER = 'gitlab-review-container';

// Storage keys
const STORAGE_PREFIX = '--gitlab'; // Using `--` to make the prefix more unique
const STORAGE_MR_ID = `${STORAGE_PREFIX}-merge-request-id`;
const STORAGE_NAME = `${STORAGE_PREFIX}-name`;
const STORAGE_EMAIL = `${STORAGE_PREFIX}-email`;
const STORAGE_COMMENT = `${STORAGE_PREFIX}-comment`;
const STORAGE_PAT = `${STORAGE_PREFIX}-pat`;

// colors — these are applied programmatically
// rest of styles belong in ./styles
const BLACK = 'rgba(46, 46, 46, 1)';
const CLEAR = 'rgba(255, 255, 255, 0)';
const MUTED = 'rgba(223, 223, 223, 0.5)';
const RED = 'rgba(219, 59, 33, 1)';
const WHITE = 'rgba(250, 250, 250, 1)';

export {
  ADD_NAME,
  ADD_NAME_BUTTON,
  ADD_EMAIL,
  ADD_NAME_CANCEL_BUTTON,
  ADD_PAT,
  ADD_PAT_BUTTON,
  ADD_PAT_CANCEL_BUTTON,
  CHANGE_MR_ID_BUTTON,
  COMMENT_BOX,
  COMMENT_BUTTON,
  EXPANSION_BUTTON,
  EXPANSION_BUTTON_CONTAINER,
  EXPANSION_CONTAINER,
  FORM,
  FORM_CONTAINER,
  MR_ID,
  MR_ID_BUTTON,
  MR_ID_CANCEL_BUTTON,
  NAME_FIELDS_CONTAINER,
  NOTE,
  NOTE_CONTAINER,
  PAT_FIELD_CONTAINER,
  REMEMBER_ITEM,
  REVIEW_CONTAINER,
  STORAGE_MR_ID,
  STORAGE_NAME,
  STORAGE_EMAIL,
  STORAGE_COMMENT,
  STORAGE_PAT,
  BLACK,
  CLEAR,
  MUTED,
  RED,
  WHITE,
};
