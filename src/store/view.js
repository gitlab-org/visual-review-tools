import { comment, mrForm, userInfoForm } from '../components';
import { localStorage, ADD_NAME, COMMENT_BOX, MR_ID, STORAGE_MR_ID } from '../shared';

const nextView = (appState, form = 'none') => {
  const formsList = {
    [ADD_NAME]: currentState => (currentState.mergeRequestId ? userInfoForm(currentState) : mrForm),
    [COMMENT_BOX]: currentState => (currentState.mergeRequestId ? comment(currentState) : mrForm),
    [MR_ID]: currentState => comment(currentState),
    none: currentState => {
      if (!currentState.mergeRequestId) {
        return mrForm;
      }

      return comment(currentState);
    },
  };

  return formsList[form](appState);
};

const getInitialView = state => {
  const mrId = localStorage.getItem(STORAGE_MR_ID);

  if (mrId) {
    state.mergeRequestId = mrId;
  }

  return nextView(state);
};

export { getInitialView, nextView };
