const path = require('path');
const CompressionPlugin = require('compression-webpack-plugin');
const webpack = require('webpack');

const ROOT_PATH = __dirname;
const CACHE_PATH = process.env.WEBPACK_CACHE_PATH || path.join(ROOT_PATH, 'tmp/cache');
const NO_SOURCEMAPS = process.env.NO_SOURCEMAPS;
const NO_COMPRESSION = process.env.NO_COMPRESSION;
const IS_PRODUCTION = process.env.NODE_ENV === 'production';
const IS_DEV_SERVER = process.argv.join(' ').indexOf('webpack-dev-server') !== -1;
const DEV_SERVER_HOST = process.env.DEV_SERVER_HOST || 'localhost';
const DEV_SERVER_PORT = parseInt(process.env.DEV_SERVER_PORT, 10) || 8080;
const DEV_SERVER_LIVERELOAD = IS_DEV_SERVER && process.env.DEV_SERVER_LIVERELOAD !== 'false';

const deployPath = IS_PRODUCTION ? 'dist' : 'dev';
const mode = IS_PRODUCTION ? 'production' : 'development';

const tool = IS_PRODUCTION ? 'source-map' : 'cheap-module-eval-source-map';
const devtool = NO_SOURCEMAPS ? false : tool;

module.exports = {
  mode,
  devtool,

  context: path.join(ROOT_PATH, 'src'),

  name: 'visual_review_toolbar',

  entry: '.',

  output: {
    path: path.join(ROOT_PATH, deployPath),
    filename: 'visual_review_toolbar.js',
    library: 'VisualReviewToolbar',
    libraryTarget: 'var',
  },

  resolve: {
    extensions: ['.js'],
    alias: {
      '~': path.join(ROOT_PATH, 'src'),
    },
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: path.join(CACHE_PATH, 'babel-loader'),
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },

  plugins: [
    // compression can require a lot of compute time and is disabled in CI
    IS_PRODUCTION && !NO_COMPRESSION && new CompressionPlugin(),
    DEV_SERVER_LIVERELOAD && new webpack.HotModuleReplacementPlugin(),
  ].filter(Boolean),

  devServer: {
    host: DEV_SERVER_HOST,
    port: DEV_SERVER_PORT,
    hot: DEV_SERVER_LIVERELOAD,
    inline: DEV_SERVER_LIVERELOAD,
    open: true,
    openPage: 'dev/index.html',
    publicPath: '/dev/',
  },
};
